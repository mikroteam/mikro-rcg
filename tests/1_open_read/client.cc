/*
 * File: client.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple client
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <string.h>

int do_open(char* f, int flags);
int do_read(int fd, char* buf, size_t nb);

static const char ref[] = "RPC rocks!\n";

int main(void)
{
  char file[] = "test.in";

  int fd = do_open(file, O_RDONLY);
  if (fd == -1)
  {
    printf("Error while opening file\n");
    return 1;
  }
  char buf[64];
  int err = do_read(fd, buf, 64);
  if (err == -1)
  {
    printf("Error while reading file\n");
    return 1;
  }

  if (strncmp(buf, ref, sizeof (ref)))
    return 1;

  return 0;
}

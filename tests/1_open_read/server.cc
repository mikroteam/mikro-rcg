/*
 * File: server.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple server
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include "rpc.hh"

int do_open(char* f, int flags)
{
  printf("do_open - %s, %d\n", f, flags);
  return open(f, flags);
}

int do_read(int fd, char* buf, size_t nb)
{
  printf("do_read - %d, %p, %lu\n", fd, buf, nb);
  return read(fd, buf, nb);
}

int main(void)
{
  rpc_serve();
  return 0;
}

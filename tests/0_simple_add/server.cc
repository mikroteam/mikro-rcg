/*
 * File: server.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple server
 *
 */

#include <stdio.h>
#include "rpc.hh"

int simple_add(int a, int b)
{
  printf("simple_add - %d, %d\n", a ,b);
  return a+b;
}

int main(void)
{
  rpc_serve();
  return 0;
}

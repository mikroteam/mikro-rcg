/*
 * File: client.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple client
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <fcntl.h>

int simple_add(int, int);

int main(void)
{
  int res = simple_add(41, 1);
  if (res != 42)
    return 1;

  return 0;
}

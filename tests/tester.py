#!/usr/bin/env python

import subprocess
import sys
import os
import os.path
import re
from functools import wraps
import errno
import signal

class TimeoutError(Exception):
  pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
  def decorator(func):

    def _handle_timeout(signum, frame):
      raise TimeoutError(error_message)

    def wrapper(*args, **kwargs):
      signal.signal(signal.SIGALRM, _handle_timeout)
      signal.alarm(seconds)
      try:
        result = func(*args, **kwargs)
      finally:
        signal.alarm(0)
      return result

    return wraps(func)(wrapper)

  return decorator

class Stats:
  testCount = 0
  testPass = 0

class tcolors:
  TestPass = "\033[32m"
  TestFail = "\033[31m"
  TestName = "\033[4;35m"
  TestWarn = "\033[33m"
  End = "\033[0m"

def abort(msg):
  print(msg)
  sys.exit(1)

@timeout(5)
def callClient(clt_name):
  return subprocess.call(clt_name)

def runTest():
  srv_name = "./server"
  srv = subprocess.Popen([srv_name])
  try:
    ret = callClient("./client")
  except TimeoutError:
    print(tcolors.TestWarn + "TIMEOUT" + tcolors.End)
    ret = 2
  finally:
    srv.kill()

  return ret

def printStats():
  print("===================================================================")
  print("")
  print("\t Overall: " + str(Stats.testPass) + " of " + str(Stats.testCount) + " passed.")
  print("")
  print("===================================================================")

def callTest(name):
  print("===================================================================")
  print(tcolors.TestName + name + tcolors.End + os.linesep)
  try:
    retCode = runTest()
  except OSError:
    print("===================================================================")
    print(tcolors.TestWarn + "=> No binary: IGNORE" + tcolors.End)
    return
  except TimeoutError:
    print("===================================================================")
    print(tcolors.TestWarn + "=> TIMEOUT" + tcolors.End)
    return


  Stats.testCount += 1
  if retCode != 0:
    print("===================================================================")
    print(tcolors.TestFail + "=> FAIL" + tcolors.End)
    return

  print("===================================================================")
  print(tcolors.TestPass + "=> PASS" + tcolors.End)
  Stats.testPass += 1

def verifyTest(folder):
  prev_folder = os.getcwd()
  os.chdir(folder)
  callTest(folder)
  os.chdir(prev_folder)

def main():
  files = os.listdir(".")

  for curr in sorted(files):
    if os.path.isdir(curr):
      verifyTest(curr)

  printStats()

if __name__ == "__main__":
  main()

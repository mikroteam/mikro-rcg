/*
 * File: mikro-ipc.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: SysV compatiblity layer with Mikro's IPC
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

#include <mikro/ipc.h>

struct ipc_msg
{
  long mtype;
  char data[IPC_MAX_SIZE];
};

static int channel_create_open(const char* name, u32 flags)
{
  char id = 0;
  int msqid;
  key_t key;

  if ((key = ftok(name, id)) == -1)
  {
    perror("ftok");
    return key;
  }

  if ((msqid = msgget(key, 0644 | flags)) == -1)
  {
    perror("msgget");
    return msqid;
  }

  return msqid;
}

int channel_create(const char* name, u32 flags)
{
  (void)flags; // Ignore flags for now
  return channel_create_open(name, IPC_CREAT);
}

int channel_close(int ch)
{
  int res;

  if ((res = msgctl(ch, IPC_RMID, NULL)) == -1)
    perror("msgctl");
  return res;
}

int channel_open(const char* name, u32 flags)
{
  (void)flags; // Ignore flags for now
  return channel_create_open(name, 0);
}

int channel_send(int ch, const void* msg, size_t size, u32 flags)
{
  (void)flags; // Ignore flags for now
  if (size > IPC_MAX_SIZE)
    return -1;

  struct ipc_msg ibuf;

  ibuf.mtype = 1;
  memcpy(ibuf.data, msg, size);

  if (msgsnd(ch, &ibuf, size, 0) == -1)
    return -1;
  return 1;
}

int channel_recv(int ch, void* msg, size_t size, u32 flags)
{
  (void)flags; // Ignore flags for now
  if (size > IPC_MAX_SIZE)
    return -1;

  struct ipc_msg ibuf;

  // FIXME: handle E2BIG
  if (msgrcv(ch, &ibuf, size, 0, 0) == -1)
    return -1;
  memcpy(msg, ibuf.data, size);
  return 1;
}

int channel_srecv(int ch, void* msg_out, size_t size_out, void* msg_in,
                  size_t size_in, u32 flags)
{
  int ret;

  (void)flags; // Ignore flags for now
  ret = channel_send(ch, msg_out, size_out, 0);
  if (ret < 0)
    return ret;

  return channel_recv(ch, msg_in, size_in, 0);
}

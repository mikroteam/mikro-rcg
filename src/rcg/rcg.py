#!/usr/bin/env python2

import os
import sys

import lexer
import parser
import gen
import functions_process
import utils

def main():
  gen_type = None

  for prg_arg in sys.argv[1:]:
    if prg_arg not in ["--client", "--server"]:
      abort("Unknown argument: " + prg_arg)
    elif prg_arg == "--client":
      gen_type = "client"
    else:
      gen_type = "server"

  if not gen_type:
    abort("You must generate code for client or server")

  tokens = list(lexer.tokenize(sys.stdin))
  funcs = parser.parseFunctions(tokens)
  functions_process.processFunctions(funcs)

  if gen_type == "client":
    gen.generateClientCode(sys.stdout, funcs)
  else:
    gen.generateServerCode(sys.stdout, funcs)

if __name__ == "__main__":
  main()

import sys

def abort(message):
  print(message)
  sys.exit(1)

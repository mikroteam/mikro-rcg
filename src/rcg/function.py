import utils

class Value:
  def __init__(self, kind, c_type):
    self.kind = kind
    self.c_type = c_type
    self.name = None
    self.specifier = None
    self.rpc_type = None

  def setName(self, name):
    if self.name:
      abort("Multiple names for argument: " + name)
    else:
      self.name = name

  def setSpecifier(self, spec):
    if self.specifier:
      abort("Multiple specifier for argument: " + spec)
    else:
      self.specifier = spec

  def isPtr(self):
    return "*" in self.c_type

MAX_ARGS = 8

class Function:
  def __init__(self, name):
    self.name = name
    self.args = []
    self.packed_args = []
    self.ret = None

  def setRet(self, v):
    if self.ret:
      abort("Multilple return value for function: " + self.name)
    else:
      self.ret = v

  def addArg(self, v):
    if len(self.args) == MAX_ARGS:
      abort("Too many arguments for function: " + self.name)
    else:
      self.args.append(v)

  def getArgSpecifier(self, name):
    for arg in self.args:
      if arg.name == name:
        return arg

  def generateArgDeclaration(self):
    res = ""
    for arg in self.args:
      res += arg.c_type + " " + arg.name + ", "
    return res[:-2]

  def generateArgPtrDeclaration(self):
    res = ""
    for arg in self.args:
      if arg.isPtr():
        res += arg.c_type + " " + arg.name + ", "
      else:
        res += arg.c_type + "* " + arg.name + ", "
    return res[:-2]

  def generateArgNameList(self):
    res = ""
    for arg in self.args:
      res += arg.name + ", "
    return res[:-2]

  def generateArgPtrNameList(self):
    res = ""
    for arg in self.args:
      if arg.isPtr():
        res += arg.name + ", "
      else:
        res += "*" + arg.name + ", "
    return res[:-2]

  def generatePackedArgList(self):
    #FIXME: Compute real dependencies (topological sort ?) or just swap members
    for arg in self.args:
      if not arg.isPtr():
        self.packed_args.append(arg)

    for arg in self.args:
      if arg.isPtr():
        self.packed_args.append(arg)

from function import Function, Value
import utils

def prettyPrint(functions):
  for f in functions:
    print(f.name)
    for arg in [f.ret] + f.args:
      print("\t"+ arg.name + " : " + arg.kind + " " + arg.c_type)

def verifySpecifier(all_values, v):
  for arg in all_values:
    if arg.name == v.specifier:
      return
  abort("Specifier: " + v.specifier + " does not match any arg")

def verifyFunction(func):
  if not func.ret:
    abort("No return value for function: " + func.name)
  all_values = func.args + [func.ret]
  for arg in all_values:
    if "*" in arg.c_type and not arg.specifier:
      if not arg.specifier:
        abort("No specifier on pointer inside function: " + func.name)
    if arg.specifier != "str":
      verifySpecifier(all_values, arg)

def verifySanity(functions):
  for func in functions:
    verifyFunction(func)

def nameArguments(functions):
  for fun in functions:
    curr_id = 0
    for arg in fun.args:
      name = "arg" + str(curr_id)
      if not arg.name:
        arg.name = name
      curr_id = curr_id + 1
    if not fun.ret.name:
      fun.ret.name = "res"

def findRPCTypes(functions):
  for fun in functions:
    for arg in fun.args:
      if arg.isPtr():
        if arg.specifier == "str":
          arg.rpc_type = "str"
        else:
          arg.rpc_type = "mem"

def generatePackedArgs(functions):
  for fun in functions:
    fun.generatePackedArgList()

def processFunctions(funcs):
  verifySanity(funcs)
  nameArguments(funcs)
  findRPCTypes(funcs)
  generatePackedArgs(funcs)

from function import Function, Value

def parseFunctions(tokens):
  functions = []
  while tokens:
    functions.append(parseFunction(tokens))
  return functions

def parseFunction(tokens):
  name = tokens.pop(0)
  sep = tokens.pop(0)

  if sep != "(":
    abort("Unexpected token: " + sep)

  new_f = Function(name)

  tok = tokens[0]
  while tok != ")":
    parseValue(new_f, tokens)
    tok = tokens[0]
  tokens.pop(0)
  return new_f

def parseValue(new_f, tokens):
  val_type = tokens.pop(0)

  new_val = Value(val_type, tokens.pop(0))
  if val_type == "ret":
    new_f.setRet(new_val)
  elif val_type in [ "in", "out" ]:
    new_f.addArg(new_val)
  else:
    abort("Unexpected token: " + val_type)

  tok = tokens[0]
  if tok in ["out", "in", "ret", ")"]:
    return
  else:
    tokens.pop(0)

  if tok == ":":
    new_val.setSpecifier(tokens.pop(0))
  else:
    new_val.setName(tok)
  return

/*
 * File: packers.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Packers for RPC
 *
 */

#ifndef PACKERS_HH_
# define PACKERS_HH_

# include <stddef.h>
# include "buffer.hh"

int pack_char(rpc_buf *buf, char c);
int pack_short(rpc_buf *buf, short n);
int pack_int(rpc_buf *buf, int n);
int pack_size_t(rpc_buf *buf, size_t n);
int pack_str(rpc_buf *buf, char* str);
int pack_mem(rpc_buf *buf, char* mem, size_t sz);

#endif /* !PACKERS_HH_ */

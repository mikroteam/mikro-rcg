/*
 * File: buffer_internal.c
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple buffer implemenation
 *
 */

#include <stdlib.h>
#include "buffer.hh"

rpc_buf *buff_new(size_t max)
{
  rpc_buf *b = NULL;

  b = (rpc_buf*)malloc(sizeof (rpc_buf));
  if (!b)
    return NULL;

  b->size = 0;
  b->max = max;
  b->data = (char*)malloc(sizeof (char) * max);
  if (!b->data)
  {
    free(b);
    return NULL;
  }

  return b;
}

void buff_release(rpc_buf *b)
{
  free(b->data);
  free(b);
}

static int buff_intern_put_data(rpc_buf *b, char c)
{
  if (b->size == b->max - 1)
  {
    b->max *= 2;
    char* new_data = (char*)realloc(b->data, sizeof(char) * b->max);
    if (!new_data)
      return -1;
    else
      b->data = new_data;
  }
  b->data[b->size++] = c;
  return 1;
}

int buff_put_data(rpc_buf *b, const void* data, size_t l)
{
  if (data == NULL && l != 0)
    return -1;

  const char* cdata = (const char*)data;

  for (size_t i = 0; i < l; i++)
    if (buff_intern_put_data(b, cdata[i]) < 0)
      return -1;
  return 1;
}

char* buff_get_data(rpc_buf *b)
{
  return b->data;
}

size_t buff_get_size(rpc_buf *b)
{
  return b->size;
}

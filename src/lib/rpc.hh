/*
 * File: lib.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: lib
 *
 */

#ifndef LIB_H_
# define LIB_H_

# include <stddef.h>
# include <stdarg.h>
# include "buffer.hh"

# define MAX_ARGS 8

typedef rpc_buf* (*clt_packer)(rpc_buf*, va_list*);
typedef size_t (*clt_unpacker)(va_list*, char*);
typedef int (*srv_unpacker)(char*, void**);
typedef rpc_buf* (*srv_packer)(size_t res, void**);

struct rpc_client_entry
{
  clt_packer pack;
  clt_unpacker unpack;
};

struct rpc_server_entry
{
  void* fun;
  srv_packer pack;
  srv_unpacker unpack;
};

void rpc_serve(void);
size_t do_rpc(size_t id, ...);

#endif /* !LIB_H_ */

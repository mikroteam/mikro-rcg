/*
 * File: buffer_internal.h
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: simple buffer implemenation
 *
 */

#ifndef BUFFER_H_
# define BUFFER_H_

# include <stddef.h>

struct rpc_buffer
{
  char *data;
  size_t max;
  size_t size;
};

typedef struct rpc_buffer rpc_buf;

/* Create a new buffer */
rpc_buf *buff_new(size_t max);

/* Add a string with size l to buffer and resize it if needed */
int buff_put_data(rpc_buf *b, const void* data, size_t l);

/* Return the pointer to the string buffer */
char *buff_get_data(rpc_buf *b);

/* Free buffer */
void buff_release(rpc_buf *b);

/* Return the size used in the buffer */
size_t buff_get_size(rpc_buf *b);

#endif /* !BUFFER_H_ */

/*
 * File: packers.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Packers for RPC
 *
 */

#include <string.h>

#include "packers.hh"

int pack_char(rpc_buf *buf, char c)
{
  return buff_put_data(buf, &c, sizeof (char));
}

int pack_short(rpc_buf *buf, short n)
{
  return buff_put_data(buf, &n, sizeof (short));
}

int pack_int(rpc_buf *buf, int n)
{
  return buff_put_data(buf, &n, sizeof (int));
}

int pack_size_t(rpc_buf *buf, size_t n)
{
  return buff_put_data(buf, &n, sizeof (size_t));
}

int pack_str(rpc_buf* buf, char* str)
{
  return buff_put_data(buf, str, strlen(str) + 1);
}

int pack_mem(rpc_buf *buf, char* mem, size_t sz)
{
  return buff_put_data(buf, mem, sz);
}

/*
 * File: lib_client.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <stdio.h>
#include <stdarg.h>

#include <mikro/ipc.h>

#include "rpc.hh"
#include "buffer.hh"
#include "unpackers.hh"

static char rpc_buff[IPC_MAX_SIZE];

extern struct rpc_client_entry rpc_functions[];

extern size_t rpc_function_nb;

size_t do_rpc(size_t id, ...)
{
  if (id >= rpc_function_nb)
  {
    printf("No such function %zx\n", id);
    return -1;
  }

  va_list begin_args;
  va_list vargs;
  rpc_buf* buf = buff_new(128);
  int ch = channel_open("/tmp/server", 0);
  int ans_ch = channel_open("/tmp/client", 0);
  size_t res = 0;

  if (ch < 0 || ans_ch < 0)
    return -1;

  va_start(vargs, id);
  va_copy(begin_args, vargs);

  if (!buff_put_data(buf, &id, sizeof (size_t)))
    goto error;

  buf = rpc_functions[id].pack(buf, &vargs);
  if (!buf)
    goto error;

  va_end(vargs);
  channel_send(ch, buff_get_data(buf), buff_get_size(buf), 0);

  if (channel_recv(ans_ch, rpc_buff, IPC_MAX_SIZE, 0) < 0)
  {
    printf("Error on channel recv");
    return -1;
  }
  res = rpc_functions[id].unpack(&begin_args, rpc_buff);

error:
  buff_release(buf);
  return res;
}

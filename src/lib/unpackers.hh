/*
 * File: unpackers.hh
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description: Unpackers for RPC
 *
 */

#ifndef UNPACKERS_HH_
# define UNPACKERS_HH_

char* unpack_char(char* buf, void** res);
char* unpack_short(char* buf, void** res);
char* unpack_int(char* buf, void** res);
char* unpack_size_t(char* buf, void** res);
char* unpack_str(char* buf, void** res);
char* unpack_mem(char* buf, void** res, size_t sz);

#endif /* !UNPACKERS_HH_ */

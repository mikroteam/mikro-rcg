/*
 * File: unpackers.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <stddef.h>

char* unpack_char(char* buf, void** res)
{
  res[0] = buf;
  return buf + sizeof (char);
}

char* unpack_short(char* buf, void** res)
{
  res[0] = buf;
  return buf + sizeof (short);
}

char* unpack_int(char* buf, void** res)
{
  res[0] = buf;
  return buf + sizeof (int);
}

char* unpack_size_t(char* buf, void** res)
{
  res[0] = buf;
  return buf + sizeof (size_t);
}

char* unpack_str(char* buf, void** res)
{
  res[0] = buf;
  for ( ; *buf; buf++)
    ;
  return buf + 1;
}

char* unpack_mem(char* buf, void** res, size_t sz)
{
  res[0] = buf;
  return buf + sz;
}

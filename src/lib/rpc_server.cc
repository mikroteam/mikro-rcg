/*
 * File: lib_server.cc
 * Author: Julien Freche <julien.freche@lse.epita.fr>
 *
 * Description:
 *
 */

#include <stdio.h>

#include <mikro/ipc.h>

#include "rpc.hh"
#include "buffer.hh"
#include "unpackers.hh"

extern struct rpc_server_entry rpc_functions[];

typedef size_t (*rpc_func)(...);

extern size_t rpc_function_nb;

static char rpc_data[IPC_MAX_SIZE];

void rpc_handler(int channel, int reply_ch)
{
  if (channel_recv(channel, rpc_data, IPC_MAX_SIZE, 0) < 0)
  {
    printf("Error on channel_recv\n");
    return;
  }

  char* ptr_data = rpc_data;
  void* args[MAX_ARGS];
  rpc_func fun;
  size_t res;
  size_t* id;

  // Unpack function id
  ptr_data = unpack_size_t(ptr_data, (void**)&id);
  if (*id >= rpc_function_nb)
  {
    printf("No such function %zx\n", *id);
    return;
  }
  fun = (rpc_func)(rpc_functions[*id].fun);

  // Unpack all args
  if (!rpc_functions[*id].unpack(ptr_data, args))
  {
    printf("Error while unpacking\n");
    return;
  }

  // Perform call
  res = fun(args[0], args[1], args[2], args[3],
            args[4], args[5], args[6], args[7]);

  // Build answer
  rpc_buf* buf = rpc_functions[*id].pack(res, args);
  if (!buf)
  {
    printf("Error while packing\n");
    return;
  }

  // Send answer
  if (channel_send(reply_ch, buff_get_data(buf), buff_get_size(buf), 0) < 0)
  {
    printf("Error while sending\n");
    return;
  }

  buff_release(buf);
}

void rpc_serve(void)
{
  int ch = channel_create("/tmp/server", CHANNEL_SERVER);
  int reply_ch = channel_create("/tmp/client", CHANNEL_ONE);

  if (ch < 0 || reply_ch < 0)
  {
    printf("Error while creating channel\n");
    return;
  }

  while (1)
  {
    rpc_handler(ch, reply_ch);
  }

  channel_close(ch);
  channel_close(reply_ch);
}

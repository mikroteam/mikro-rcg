# Makefile

-include Makefile.rules

SRC_N=buffer.cc packers.cc unpackers.cc rpc_client.cc rpc_server.cc
SRCS_LIB=$(addprefix src/lib/, $(SRC_N))
SRCS=$(SRCS_LIB) src/sysv_compat/ipc.cc
OBJS=$(SRCS:.cc=.o)
DEPS=$(SRCS:.cc=.d)
LIBRCG=librcg.a

all: $(LIBRCG)

$(LIBRCG): $(OBJS)
	$(AR) $(ARFLAGS) $@ $^

test: all
	$(MAKE) -I . -C tests

clean:
	$(MAKE) -I . -C tests clean
	$(RM) $(DEPS) $(OBJS) $(LIBRCG)

distclean: clean
	$(RM) Makefile.rules

Makefile.rules:
	./configure

-include $(DEPS)
